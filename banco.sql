DROP DATABASE IF EXISTS engSoft;
CREATE DATABASE engSoft;

USE engSoft;

-- 0 ADMIN
-- 1 EMPRESA
-- 2 CLIENTE

CREATE TABLE usuario
(
  id BIGINT(20) NOT NULL AUTO_INCREMENT,
--  username VARCHAR(30) UNIQUE,
  email VARCHAR(200) UNIQUE,
  senha VARCHAR(255),
  tipoUsuario INTEGER,
  PRIMARY KEY (id)
);

CREATE TABLE empresa
(
  cnpj BIGINT(20) NOT NULL AUTO_INCREMENT,
  nome VARCHAR(200) NOT NULL,
  telefone BIGINT(20) NOT NULL,
  estado VARCHAR(200) NOT NULL,
  cidade VARCHAR(200) NOT NULL,
  endereco VARCHAR(200) NOT NULL,
  PRIMARY KEY (cnpj),
  FOREIGN KEY (cnpj) REFERENCES usuario(id)
);

CREATE TABLE cliente
(
  cpf BIGINT(20) NOT NULL AUTO_INCREMENT, -- id
  telefone BIGINT(20) NOT NULL,
  nome VARCHAR(200) NOT NULL,
  estado VARCHAR(200) NOT NULL,
  cidade VARCHAR(200) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (id) REFERENCES usuario(id)
);

CREATE TABLE categoria
(
  id INTEGER NOT NULL AUTO_INCREMENT,
  nome VARCHAR(200) NOT NULL,
  categoriaPaiId INTEGER DEFAULT 0,
  PRIMARY KEY (id),
  FOREIGN KEY (categoriaPaiId) REFERENCES categoria(id)
);

CREATE TABLE escolaridade
(
  id INTEGER NOT NULL AUTO_INCREMENT,
  nome VARCHAR(200) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE estagio
(
  id BIGINT(20) NOT NULL AUTO_INCREMENT,
  idEmpresa BIGINT(20) NOT NULL,
  funcao VARCHAR(200) DEFAULT NULL,
  descricao VARCHAR(200) DEFAULT NULL,
  inicioOferta DATE,
  fimOferta DATE,
  quantidadeVagas INT,
  escolaridadeRequeridaId INT,
  categoriaId INT,
  PRIMARY KEY (id),
  FOREIGN KEY (idEmpresa) REFERENCES empresa(cnpj),
  FOREIGN KEY (escolaridadeRequeridaId) REFERENCES escolaridade(id),
  FOREIGN KEY (categoriaId) REFERENCES categoria(id)
);

CREATE TABLE clienteEstagio
(
  idEstagio BIGINT(20) NOT NULL,
  idCliente BIGINT(20) NOT NULL,
  dataInteresse DATETIME DEFAULT NOW(),
  PRIMARY KEY (idEstagio, idCliente),
  FOREIGN KEY (idEstagio) REFERENCES estagio(id),
  FOREIGN KEY (idCliente) REFERENCES cliente(id)
);

CREATE TABLE logAcesso
(
  id BIGINT NOT NULL AUTO_INCREMENT,
  idUsuario BIGINT(20) NOT NULL,
  tipoUsuario INT NOT NULL,
  acao VARCHAR(255) NOT NULL,
  dataAcao DATETIME DEFAULT NOW(),
  PRIMARY KEY (id),
  FOREIGN KEY (idUsuario) REFERENCES usuario(id)
);

create database TerceiraUnidade;
drop table artefato;
create table artefato(
codigo bigint not null auto_increment,
nome varchar(40),
dataCriacao varchar(30),
localDescoberta varchar(30),
anoDescoberta int(5),
descricao varchar(80),
primary key(Codigo)
)default charset=utf8;
select * from artefato;


CREATE TABLE cliente (
id bigint(20) NOT NULL AUTO_INCREMENT,
nome varchar(120) DEFAULT NULL,
email varchar(60) DEFAULT NULL,
endereco varchar(70) DEFAULT NULL,
dataNascimento varchar(70) DEFAULT NULL,
usuario varchar(70) DEFAULT NULL,
senha varchar(70) DEFAULT NULL,
PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

create database Registro;

CREATE TABLE cliente (
id bigint(20) NOT NULL AUTO_INCREMENT,
nome varchar(120) DEFAULT NULL,
email varchar(60) DEFAULT NULL,
endereco varchar(70) DEFAULT NULL,
dataNascimento varchar(70) DEFAULT NULL,
usuario varchar(70) DEFAULT NULL,
senha varchar(70) DEFAULT NULL,
PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

CREATE TABLE empresa
(
  cnpj BIGINT(20) NOT NULL AUTO_INCREMENT,
  nome VARCHAR(200) NOT NULL,
  telefone BIGINT(20) NOT NULL,
  estado VARCHAR(200) NOT NULL,
  cidade VARCHAR(200) NOT NULL,
  endereco VARCHAR(200) NOT NULL,
  PRIMARY KEY (cnpj),
  FOREIGN KEY (cnpj) REFERENCES usuario(id)
);

create database engenhariaSoft;

	CREATE TABLE empresas
	(
		cnpj BIGINT(20) NOT NULL AUTO_INCREMENT,
		nome VARCHAR(200) NOT NULL,
		email VARCHAR(200) NOT NULL,
		senha VARCHAR(200) NOT NULL,
		estado VARCHAR(200) NOT NULL,
		cidade VARCHAR(200) NOT NULL,
		endereco VARCHAR(200) NOT NULL,
		PRIMARY KEY (cnpj)
	);

	CREATE TABLE estagios
	(
		id BIGINT(20) NOT NULL AUTO_INCREMENT,
		cnpjEmpresa BIGINT(20) NOT NULL,
		funcao VARCHAR(200) DEFAULT NULL,
		descricao VARCHAR(200) DEFAULT NULL,
		Inioferta VARCHAR(200) DEFAULT NULL,
		Fimoferta VARCHAR(200) DEFAULT NULL,
		cidadeEmpresa VARCHAR(200) DEFAULT NULL,
		estadoEmpresa VARCHAR(200) DEFAULT NULL,
		quantidadeVagas INT,
		PRIMARY KEY (id),
		FOREIGN KEY (cnpjEmpresa) REFERENCES empresas(cnpj)
	);

	CREATE TABLE clientes(
		cpf BIGINT(30) NOT NULL,
		nome varchar(120) DEFAULT NULL,
		email varchar(60) NOT NULL,
		dataNascimento varchar(70) DEFAULT NULL,
		endereco varchar(70) DEFAULT NULL,
		estado VARCHAR(200) NOT NULL,
		cidade VARCHAR(200) NOT NULL,
		senha varchar(70) NOT NULL,
		PRIMARY KEY (cpf)
	);
drop table estagios;
drop table clientes;
select * from estagios;
select * from empresas;
select * from clientes;