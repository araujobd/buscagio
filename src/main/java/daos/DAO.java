package daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by bruno on 09/06/17.
 */
public class DAO {

    protected Connection conexao;
    protected PreparedStatement statement;
    protected ResultSet resultSet;
    protected String sql;


    void prepareSql(String sql) throws SQLException {
        statement = conexao.prepareStatement(sql);
    }

    void execute() throws SQLException {
        resultSet = statement.executeQuery();
    }
}
