package daos;

import model.Usuario;

import javax.inject.Inject;
import java.sql.Connection;

/**
 * Created by bruno on 09/06/17.
 */
public class UsuarioDAO extends DAO {

    protected UsuarioDAO() {

    }

    @Inject
    public UsuarioDAO(Connection conexao) {
        this.conexao = conexao;
    }


    public Usuario logar(Usuario usuario) {
        sql = "SELECT * FROM " + usuario + "WHERE email = ? and senha = ?";
        return usuario;
    }
}
