/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Result;
import javax.inject.Inject;

/**
 *
 * @author cristovao
 */
@Controller
@Path("/empresa")
public class EmpresaController {
    //    private ClienteDAO dao;
    private Result result;

    /**
     * @deprecated CDI EYES ONLY
     */
    protected EmpresaController() {
        //this();
    }

    /**
     * sadas
     */
    @Inject
    public EmpresaController(Result result) {
        this.result = result;
//        this.dao = dao;
    }

    public void home() {

    }

    public void perfil() {

    }
    @Path("/cadastro")
    public void cadastro() {

    }
    @Path("/editar")
    public void editar() {

    }
}
