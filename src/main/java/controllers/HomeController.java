/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import dao.UsuarioDAO;
import model.Usuario;

import javax.inject.Inject;

/**
 *
 * @author bruno
 */
@Controller
public class HomeController {

  private Result result;
  private Usuario usuario;
  private UsuarioDAO dao;
  private boolean logado = false;
  private enum tipo{CLIENTE, EMPRESA};

  /**
   * @deprecated
   */
  protected HomeController() {
    this(null, null);
  }

  @Inject
  public HomeController(Result result, UsuarioDAO dao) {
    this.result = result;
    this.usuario = new Usuario();
    this.dao = dao;
  }

  @Path("/")
  public void index() {
    result.include("usuario", this.usuario);
    if (logado) {
      result.redirectTo(HomeController.class).pass();
    }
  }

  private void pass() {
  }

  @Path("/login")
  public void login() {

  }

  @Post("/logar")
  public void logar(Usuario usuario) {
/*
    Usuario logar = dao.logar(hogin, senha);

    if (logar != null) {
      result.redirectTo(HomeController.class).index();

    } else {
      result.include("mensagem_error", "Login ou Senha inválidos!");
      result.redirectTo(HomeController.class).login();
    }*/
  }
}
