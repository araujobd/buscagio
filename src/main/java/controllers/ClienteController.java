/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Result;
import dao.ClienteDAO;

import javax.inject.Inject;

/**
 * @author bruno
 */
@Controller
@Path("/cliente")
public class ClienteController {

//    private ClienteDAO dao;
    private Result result;

    /**
     * @deprecated CDI EYES ONLY
     */
    protected ClienteController() {
        //this();
    }

    /**
     * sadas
     */
    @Inject
    public ClienteController(Result result) {
        this.result = result;
//        this.dao = dao;
    }

    public void home() {

    }

    public void perfil() {

    }
    @Path("/cadastro")
    public void cadastro() {

    }
    @Path("/editar")
    public void editar() {

    }

}
