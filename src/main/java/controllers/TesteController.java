/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Result;
import dao.implementacao.UsuarioDAOImpl;
import entidades.ss.Usuario;
import javax.inject.Inject;

/**
 *
 * @author bruno
 */

@Controller
public class TesteController {
  
  private Result result;

  private UsuarioDAOImpl dao;
  
  @Deprecated
  protected TesteController () { }

  @Inject
  public TesteController (Result result, UsuarioDAOImpl dao) {
    this.result = result;
    this.dao = dao;
  }

  @Path("/home")
  public void home() {
    String login = "bruno";
    String senha = "123456";
    Usuario u;
    u = dao.logar(login, senha);
    
    if (u != null)
      System.out.println(u + " + " + u.getId() + "+" + u.getTipoUsuario());
    else
      System.out.println(u);
//    result.include("mensagem", "Teste!!!!!");
  }
  
  @Path("/indexx")
  public void index() {
    System.out.println();
  }
}
