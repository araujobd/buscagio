package model;

import java.util.List;

/**
 * Created by bruno on 23/05/17.
 */
public class Usuario {

    private long id;
    private String email;
    private String senha;

    private String nome;
    private long telefone;
    private String estado;
    private String cidade;
    private String endereco;

    private List<LogAcesso> logs;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public long getTelefone() {
        return telefone;
    }

    public void setTelefone(long telefone) {
        this.telefone = telefone;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public List<LogAcesso> getLogs() {
        return logs;
    }

    public void setLogs(List<LogAcesso> logs) {
        this.logs = logs;
    }
}
