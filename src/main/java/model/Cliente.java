package model;

import java.io.Serializable;

/**
 * Created by bruno on 23/05/17.
 */
public class Cliente extends Usuario implements Serializable {

    private String dataNascimento;

    public String getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public long getCPF() {
        return this.getId();
    }

    public void setCPF(long cpf) {
        setId(cpf);
    }
}
