package model;

import java.util.Date;

/**
 * Created by bruno on 23/05/17.
 */
public class LogAcesso {

    private long id;
    private String acao;

    private Date dataAcao;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAcao() {
        return acao;
    }

    public void setAcao(String acao) {
        this.acao = acao;
    }

    public Date getDataAcao() {
        return dataAcao;
    }

    public void setDataAcao(Date dataAcao) {
        this.dataAcao = dataAcao;
    }
}
