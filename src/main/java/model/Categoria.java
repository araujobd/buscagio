package model;

import java.io.Serializable;

/**
 * Created by bruno on 23/05/17.
 */
public class Categoria implements Serializable {

    private long id;
    private String nome;

    private Categoria pai;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Categoria getPai() {
        return pai;
    }

    public void setPai(Categoria pai) {
        this.pai = pai;
    }
}
