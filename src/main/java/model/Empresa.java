package model;

import java.io.Serializable;

/**
 * Created by bruno on 23/05/17.
 */
public class Empresa extends Usuario implements Serializable {

    public long getCNPJ() {
        return getId();
    }

    public void setCNPJ(long cnpj) {
        setId(cnpj);
    }
}
