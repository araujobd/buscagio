package model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by bruno on 23/05/17.
 */
public class Estagio implements Serializable {

    private long id;
    private String funcao;
    private String descricao;
    private Integer vagas;
    private Date inicioOferta;
    private Date fimOferta;

    private Empresa empresa;
    private Escolaridade escolaridade;
    private Categoria categoria;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFuncao() {
        return funcao;
    }

    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getVagas() {
        return vagas;
    }

    public void setVagas(Integer vagas) {
        this.vagas = vagas;
    }

    public Date getInicioOferta() {
        return inicioOferta;
    }

    public void setInicioOferta(Date inicioOferta) {
        this.inicioOferta = inicioOferta;
    }

    public Date getFimOferta() {
        return fimOferta;
    }

    public void setFimOferta(Date fimOferta) {
        this.fimOferta = fimOferta;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Escolaridade getEscolaridade() {
        return escolaridade;
    }

    public void setEscolaridade(Escolaridade escolaridade) {
        this.escolaridade = escolaridade;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }
}
