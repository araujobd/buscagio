package model;

import java.io.Serializable;

/**
 * Created by bruno on 23/05/17.
 */
public class Escolaridade implements Serializable{

    private long id;
    private String nome;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
