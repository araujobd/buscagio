/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package confs;

import br.com.caelum.vraptor.http.FormatResolver;
import br.com.caelum.vraptor.view.DefaultPathResolver;
import javax.enterprise.inject.Specializes;
import javax.inject.Inject;

/**
 *
 * @author bruno
 */
@Specializes
public class CustomPathResolver extends DefaultPathResolver {

  public CustomPathResolver() {
    this(null);
  }

  @Inject
  public CustomPathResolver(FormatResolver resolver) {
    super(resolver);
  }

  /**
   *
   * @return
   */
  @Override
  protected String getPrefix() {
    return "/WEB-INF/views/";
  }

  /**
   *
   * @return
   */
  @Override
  protected String getExtension() {
    return "ftl";
  }
}
