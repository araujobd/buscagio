/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package confs;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author bruno
 */
public class FabricaJPA {

  @PersistenceContext
  private EntityManager manager;

  @Produces
  public EntityManager getManager() {
    return manager;
  }

}
