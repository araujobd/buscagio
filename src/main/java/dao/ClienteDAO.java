/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entidades.ss.Cliente;
import java.util.List;

/**
 *
 * @author bruno
 */
public interface ClienteDAO {
  
  void adicionar(Cliente cliente);
  
  Cliente buscar(String id);
  
  List<Cliente> todos();
  
  void remover(Cliente cliente);
}
