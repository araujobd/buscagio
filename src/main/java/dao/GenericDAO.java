/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

/**
 *
 * @author bruno
 */
public abstract class GenericDAO<T> {

  protected Class<T> tipo = retornaTipo();
  protected EntityManager manager;

  public void adicionar(T objeto) {
    manager.persist(objeto);
  }

  public void atualizar(T objeto) {
    manager.merge(objeto);
  }

  public void remover(T objeto) {
    manager.remove(objeto);
  }

  public List<T> todos() {
    CriteriaQuery<Object> query = manager.getCriteriaBuilder().createQuery();
    query.select(query.from(tipo));
    return (List<T>) manager.createQuery(query).getResultList();
  }

  public T buscar(long id) {
    return manager.find(tipo, id);
  }

  /**
   * @author bruno
   *
   * Utilizando Exemplo de Eduardo Guerra!
   * https://groups.google.com/forum/#!topic/projeto-oo-guiado-por-padroes/pOIiOD9cifs
   *
   * Este método retorna o tipo da Classe, dessa maneira não é necessário cada
   * Service expor seu tipo!!!!
   *
   * @return Class<T>
   */
  @SuppressWarnings({"unchecked"})
  private Class<T> retornaTipo() {
    Class<?> clazz = this.getClass();

    while (!clazz.getSuperclass().equals(GenericDAO.class)) {
      clazz = clazz.getSuperclass();
    }

    ParameterizedType tipoGenerico = (ParameterizedType) clazz.getGenericSuperclass();
    return (Class<T>) tipoGenerico.getActualTypeArguments()[0];
  }
}
