/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.implementacao;

import dao.UsuarioDAO;
import entidades.ss.Usuario;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.transaction.Transactional;

/**
 *
 * @author bruno
 */
@Transactional
public class UsuarioDAOImpl implements UsuarioDAO {

  private EntityManager manager;

  protected UsuarioDAOImpl() {
    this(null);
  }

  @Inject
  public UsuarioDAOImpl(EntityManager manager) {
    this.manager = manager;
  }

  @Override
  public Usuario logar(String login, String senha) {
    try {
      return (Usuario) manager
          .createQuery("SELECT u FROM Usuario u WHERE (u.username = :login  or u.email = :login) and u.senha = :senha", Usuario.class)
          .setParameter("login", login)
          .setParameter("senha", senha)
          .getSingleResult();
    } catch (NoResultException e) {
      return null;
    }
  }

//  @Override
//  public void adicionar(Usuario usuario) {
//    if (usuario.getId() == null) {
//      this.manager.persist(usuario);
//    } else {
//      this.manager.merge(usuario);
//    }
//  }
//  @Override
//  public void remover(Usuario usuario) {
//    this.manager.remove(usuario);
//  }
}
