/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.implementacao;

import dao.ClienteDAO;
import entidades.ss.Cliente;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

/**
 *
 * @author bruno
 */
@Transactional
public class ClienteDAOImpl implements ClienteDAO {

    private EntityManager manager;

    @Deprecated
    protected ClienteDAOImpl() {
        this(null);
    }

    @Inject
    public ClienteDAOImpl(EntityManager manager) {
        this.manager = manager;
    }

    @Override
    public void adicionar(Cliente cliente) {
        if (cliente.getId() == null) {
            this.manager.persist(cliente);
        } else {
            this.manager.merge(cliente);
        }
    }

    @Override
    public Cliente buscar(String id) {
        return (Cliente) this.manager.find(Cliente.class, id);
    }

    @Override
    public List<Cliente> todos() {
        return this.manager.createQuery("SELECT c FROM Cliente c", Cliente.class).getResultList();
    }

    @Override
    public void remover(Cliente cliente) {
        this.manager.remove(cliente);
    }

}
