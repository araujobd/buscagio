/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.implementacao;

import dao.GenericDAO;
import entidades.ss.Empresa;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author bruno
 */
@Stateless
public class EmpresaDAOImpl extends GenericDAO<Empresa> {

  /**
   * @deprecated CDI eyes only
   */
  protected EmpresaDAOImpl() {
  }

  @Inject
  public EmpresaDAOImpl(EntityManager manager) {
    this.manager = manager;
  }

}
