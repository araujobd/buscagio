<#import "../layout/layout.ftl" as layout>

<@layout.layout_padrao title="CADASTRO" header="header.ftl">
      <div class="section"></div><div class="section"></div>
        <div class="container">
          <div class="card-panel grey lighten-3 row z-depth-3" >
            <div class="row">
              <div class="input-field col s6">
                 
                    <input id="funcao" type="number" class="validate">
                    <label for="icon_prefix">Função</label>
                </div>
              
              <div class="input-field col s6">
                 
                    <input id="descricao" type="text" class="validate">
                    <label for="icon_prefix">Descrição</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    
                    <input  id="InicioOferta" type="date" class="datepicker">
                    <!--script>
                        $('.datepicker').pickadate({
                        selectMonths: true, // Creates a dropdown to control month
                        selectYears: 15 // Creates a dropdown of 15 years to control year
                        });
                    </script-->
                    <label for="icon_telephone">Inicio da Oferta</label>
                </div>
                <div class="input-field col s6">
               
                    <input id="FimOferta" type="text" class="datepicker">
                    <script>
                        $('.datepicker').pickadate({
                        selectMonths: true, // Creates a dropdown to control month
                        selectYears: 15 // Creates a dropdown of 15 years to control year
                        });
                    </script>
                    <label for="FimOferta">Fim da Oferta</label>
                </div>
            </div>
      
            <div class="row">
                <div class="input-field col s6">
       
                    <input id="vagas" type="number" class="validate">
                    <label for="icon_telephone">Vagas</label>
                </div>
                <div class="input-field col s6">
                   
                    <input id="escolaridade" type="tel" class="validate">
                    <label for="icon_telephone">Escolaridade</label>
                </div>
            </div>
              
              <div class="row">
                  
                <div class="input-field col s6">
             
                    <input id="end" type="text" class="validate" value="">
                    <label for="icon_telephone">Categoria</label>
                </div>
                <div class="input-field col s6">
                  
                    <input id="password" type="password" class="validate">
                    <label for="password">Senha</label>
                </div>
              </div>
              
            
            <a class="waves-effect waves-light btn indigo"><i class="material-icons left">cloud</i>Cadastrar estágio</a>
            <a class="waves-effect waves-light btn indigo">voltar</a>
            
        </div>
          </div>
</@layout.layout_padrao>