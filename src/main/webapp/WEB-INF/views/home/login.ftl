<#ftl strip_whitespace = true>

<#assign charset="UTF-8">
<#assign title="Example">

<!DOCTYPE html>
<html>
    <head>
        <title>${title}</title>
        <meta charset="${charset}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>
        </head>
    <body class="blue-grey lighten-2">

        <div class="main">
            <div class="section"></div><div class="section"></div><div class="section"></div><div class="section"></div>
            <div class="container">

                <div class="row">
                    <div class="col s4 z-depth-4 offset-s4 indigo lighten-5 card-panel">
                        <form action="logar" method="POST">
                            <div class="col s10 offset-s1">

                                <div class="section"></div><div class="row center-align"><h5>Buscagio</h5></div>
                                <div class="section">
                                    <#if mensagem_error ??>
                                    <div class="row center-align">${mensagem_error}</div>
                                    </#if>
                                    </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="login" name="login" type="text" class="validate" required/>
                                        <label for="login">Usuário ou Email</label>
                                        </div>
                                    </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="senha" name="senha" type="password" class="validate" required/>
                                        <label for="senha">Senha</label>
                                        </div>
                                    </div>
                                <div class="row">
                                    <label class="right ">
                                        <a class="" href='#!'><b>Esqueceu a senha?</b></a>
                                        </label>
                                    </div>
                                <div class="row">
                                    <button class="btn waves-effect waves-light col s12" type="submit">
                                        Entrar!
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                <div class="row ">

                    <a class="col s12 center-align pink-text" href="#!"><strong>Criar Conta</strong></a>

                    </div>
                </div>
            </div>

        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="js/bin/materialize.min.js"></script>

        </body>
    </html>
