<#import "../layout/layout.ftl" as layout>

<@layout.layout_padrao title="HOME" header="header.ftl">
    <div class="section"></div><div class="section"></div><div class="section"></div>
    <div class="container ">
        <div class="row">
            <div class="container offset-l2 col 10 s15">
                <table class="highlight">
                    <caption><h5>Minhas ofertas de estágio</h5></caption>
                    <thead>
                        <tr>
                            <th>Estágio</th>
                            <th>Vagas</th>
                            <th>Inscritos</th>
                            <th>Ação</th>
                            <th>Informação</th>
                        </tr>
                    </thead>

                    <tbody>
                    <tr>
                        <td>Programador Júnior</td>
                        <td>2</td>
                        <td>20</td>
                        <td><a href='#!'>Cancelar estágio</a></td>
                        <td><a href='#!'>Comunicar inscritos</a></td>
                    </tr>
                    <tr>
                        <td>Programador Júnior</td>
                        <td>2</td>
                        <td>20</td>
                        <td><a href='#!'>Cancelar estágio</a></td>
                        <td><a href='#!'>Comunicar inscritos</a></td>
                    </tr>
                    <tr>
                        <td>Programador Júnior</td>
                        <td>2</td>
                        <td>20</td>
                        <td><a href='#!'>Cancelar estágio</a></td>
                        <td><a href='#!'>Comunicar inscritos</a></td>
                    </tr>
                        <tr>
                            <td>Programador Júnior</td>
                            <td>2</td>
                            <td>20</td>
                            <td><a href='#!'>Cancelar estágio</a></td>
                            <td><a href='#!'>Comunicar inscritos</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</@layout.layout_padrao>