<div class="nav-wrapper">
    <div class="container">
        <a href="#" class="brand-logo">Buscagio</a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a href="#!">Estágios</a></li>
            <li><a href="#!">Perfil</a></li>
            <li><a href="#!">Sair</a></li>
        </ul>
    </div>
</div>