<#macro layout_padrao title header="header.ftl">
<!DOCTYPE html>
<html>
    <head>
        <title>${title}</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="/buscagio/css/materialize.css" media="screen,projection"/>
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="/buscagio/js/bin/materialize.min.js"></script>
        <style type="text/css">
            body {
                display: flex;
                min-height: 100vh;
                flex-direction: column;
            }

            main {
                flex: 1 0 auto;
            }
        </style>
    </head>

    <body>
        <nav>
            <#include header/>
        </nav>

        <main>
            <#nested />
        </main>

        <footer class="page-footer">
            <#include "footer.ftl"/>
        </footer>

    </body>
</html>
</#macro>