<#import "../layout/layout.ftl" as layout>

<@layout.layout_padrao title="HOME" header="header.ftl">
          <div class="section"></div><div class="section"></div><div class="section"></div>
    <div class="container">
        <div class="row">
            <div class="container offset-l3 col l6 s12">
                <table class="highlight">
                    <thead>
                        <tr>
                            <th>Estágio</th>
                            <th>Vagas</th>
                            <th>Inscritos</th>
                            <th>Ações</th>
                        </tr>
                    </thead>

                    <tbody>
                    <tr>
                        <td>Programador Júnior</td>
                        <td>2</td>
                        <td>20</td>
                        <td><a href='#!'>Cancelar Inscrição</a></td>
                    </tr>
                    <tr>
                        <td>Programador Júnior</td>
                        <td>2</td>
                        <td>20</td>
                        <td><a href='#!'>Cancelar Inscrição</a></td>
                    </tr>
                    <tr>
                        <td>Programador Júnior</td>
                        <td>2</td>
                        <td>20</td>
                        <td><a href='#!'>Cancelar Inscrição</a></td>
                    </tr>
                        <tr>
                            <td>Programador Júnior</td>
                            <td>2</td>
                            <td>20</td>
                            <td><a href='#!'>Cancelar Inscrição</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</@layout.layout_padrao>