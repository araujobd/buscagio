<#import "../layout/layout.ftl" as layout>

<@layout.layout_padrao title="EDITAR" header="header.ftl">
<div class="section"></div><div class="section"></div>
        <div class="container">
          <div class="card-panel grey lighten-3 row z-depth-3" >
            <div class="row">
                <div class="input-field col s6">
               
                    <input id="email" type="text" class="validate">
                    <label for="email">Email</label>
                </div>
                <div class="input-field col s6">
                 
                    <input id="telefone" type="number" class="validate">
                    <label for="telefone">Telefone</label>
                </div>
            </div>
      
            <div class="row">
                <div class="input-field col s6">
          
                    <input id="cidade" type="text" class="validate">
                    <label for="cidade">Cidade</label>
                </div>
                <div class="input-field col s6">
             
                    <input id="estado" type="text" class="validate">
                    <label for="estado">Estado</label>
                </div>
            </div>
      
            <div class="row">
                <div class="input-field col s6">
       
                    <input id="novasenha" type="password" class="validate">
                    <label for="novasenha">Nova senha</label>
                </div>
                
                <div class="input-field col s6">
           
                    <input id="password" type="password" class="validate">
                    <label for="password">Confimar senha</label>
                </div>
                
                
            </div>
            
            <a class="waves-effect waves-light btn indigo"><i class="material-icons left">cloud</i>Salvar</a>
            <a class="waves-effect waves-light btn indigo">voltar</a>
            
        </div>
          </div>
</@layout.layout_padrao>